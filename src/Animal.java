
public class Animal {
	private boolean swim;
	private boolean wing;
	private boolean tail;
	private boolean gill;
	private boolean fur;
	private int leg;
	private int arm;
	private double height;
	private double weight;
	public Animal() {
		this.swim = true;
		this.wing = false;
		this.tail = false;
		this.gill = false;
		this.fur = false;
		this.leg = 2;
		this.arm = 2;
		this.height = 6;
		this.weight = 150;
	}
	public Animal (boolean swims, boolean wings, boolean tails, boolean gills, boolean furs, int legs, int arms, double heights, double weights) {
		this.swim = swims;
		this.wing = wings;
		this.tail = tails;
		this.gill = gills;
		this.fur = furs;
		this.leg = legs;
		this.arm = arms;
		this.height = heights;
		this.weight = weights;
	}
	public boolean getSwim() {
		return this.swim;
	}
	public boolean getWing() {
		return this.wing;
	}
	public boolean getTail() {
		return this.tail;
	}
	public boolean getGill() {
		return this.gill;
	}
	public boolean getFur() {
		return this.fur;
	}
	public int getLeg() {
		return this.leg;
	}
	public int getArm() {
		return this.arm;
	}
	public double getHeight() {
		return this.height;
	}
	public double getWeight() {
		return this.weight;
	}
	public void setSwim(boolean swimz) {
		this.swim=swimz;
	}
	public void setWing(boolean wingz) {
		this.wing=wingz;
	}
	public void setTail(boolean tailz) {
		this.tail=tailz;
	}
	public void setGill(boolean gillz) {
		this.gill=gillz;
	}
	public void setFur(boolean furz) {
		this.fur=furz;
	}
	public void setLeg(int legz) {
		this.leg = legz;
	}
	public void setArm(int armz) {
		this.arm = armz;
	}
	public void setHeight(double heightz) {
		this.height = heightz;
	}
	public void setWeight(double weightz) {
		this.weight = weightz;
	}
	public String toString() {
		return "Your animal is "+height+" feet tall and weighs around "+weight+" pounds.  It "+(this.swim?"swims":"does not swim")+", it "+(this.wing?"can fly":"can not fly")+", it "+(this.tail?"has a tail":"does not have a tail")+", it "+(this.gill?"has gills":"does not have gills")+", it "+(this.fur?"has fur":"does not have fur")+", and finally it has "+arm+" arms and "+leg+" legs.";
	}
}
