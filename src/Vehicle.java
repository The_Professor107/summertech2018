
public class Vehicle {
	private String color;
	private int numSeats;
	private int numWheels;
	private int distance= 0;
	private int speed;
	
	public Vehicle() {
		this.color = "blue";
		this.numSeats = 1;
		this.numWheels = 2;
		this.speed = 100;
	}
	
	public Vehicle(String col, int seats, int wheels, int speed) {
		this.color = col;
		this.numSeats = seats;
		this.numWheels = wheels;
		this.speed = speed;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public int getSeats() {
		return this.numSeats;
	}
	
	public int getWheels() {
		return this.numWheels;
	}
	
	public int getSpeed(){
		return this.speed;
	}
	
	public void setColor(String newColor) {
		this.color = newColor;
	}
	
	public void setSeats(int newSeats) {
		this.numSeats = newSeats;
	}
	
	public void setWheels(int neWheels) {
		this.numWheels = neWheels;
	}
	
	public void setSpeed(int newSpeed) {
		this.speed = newSpeed;
	}
	
	public void drive(int miles) {
		this.distance += miles;
	}
	
	public void drive(double hours) {
		this.distance += hours*speed;
	}
	
	public int getDrive(){
		return this.distance;
	}
	public String toString() {
		return " is a "+this.color+" vehicle, with "+this.numSeats+" seats, and "+this.numWheels+" wheels.";
	}
}