import java.util.ArrayList;

public class War2 {

	public static void main(String[] args) {
		Deck cards = new Deck();
		cards.fillDeck();
		Deck player1 = new Deck();
		Deck player2 = new Deck();
		for(int i=0; i<cards.countCards(); i++) {
			Card pick1 = cards.pickCard();
			player1.addCard(pick1);
			cards.removeCard(pick1);
			Card pick2 = cards.pickCard();
			player2.addCard(pick2);
			cards.removeCard(pick2);
		}
		int round = 0;
		while(player1.countCards()>0 && player2.countCards()>0) {
			round++;
			Card pick1;
			Card pick2;
			System.out.println("Round "+round+": ");
			do {
				pick1 = player1.pickCard();
				pick2 = player2.pickCard();
				System.out.println("Player 1 picked "+pick1);
				System.out.println("Player 2 picked "+pick2);
				System.out.println("Player 1 has "+player1.countCards()+" card(s).");
				System.out.println("Player 2 has "+player2.countCards()+" card(s).");
				
				if(pick1.getNumber() > pick2.getNumber()) {
					player1.addCard(pick2);
					player2.removeCard(pick2);
				}else if(pick1.getNumber() > pick2.getNumber()) {
					player2.addCard(pick1);
					player1.removeCard(pick1);
				}else {
					System.out.println("War!");
					ArrayList<Card> temp = new ArrayList<Card>();
					temp.add(pick1);
					temp.add(pick2);
					for(int i=0; i<4; i++) {
						temp.add(player1.pickCard());
						temp.add(player2.pickCard());
					}

				}
			}while(pick1.getNumber()==pick2.getNumber());
		}
		if(player1.countCards()==0) {
			System.out.println("Player 2 Wins!");
		}else {
			System.out.println("Player 1 Wins!");
		}
	}

}
