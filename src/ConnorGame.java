//Connor Karr
//July 3, 2018
//ConnorGame.java

//Imports everything I need
import java.util.*;
import java.util.Scanner;
import java.util.Random;
public class ConnorGame
{
	public static void main(String args[])
	{
		//Declare the scanner and random number generator
		Scanner scan = new Scanner(System.in);
		Random rand = new Random();
		boolean playAgain=true;
		while(playAgain==true){
			String penType;
			System.out.println("Welcome to the one, the only, Hangman!");
			System.out.println("First things first, what color pen?");
			System.out.println("Blue Pen - 1");
			System.out.println("Rainbow Pen - 2");
			System.out.println("Blue Wiggle Pen - 3");
			System.out.println("Rainbow Wiggle Pen - 4");
			System.out.println("Upside Down Blue Pen - 5");
			penType = scan.next();
			
			//Checks for invalid answers by checking the value
			//It is a String because I dont want you to crash it by typing words
			while((!penType .equals ("1")) && (!penType.equals ("2")) && (!penType.equals ("3")) && (!penType.equals ("4")) && (!penType .equals("5"))){
				System.out.println("Invalid Answer");
				penType = scan.next();
			}
			
			//Creates all the pens and the window
			
			//Strings hold the dashes and the word. Also what letter you guess
			String secretWord = "";
			String dashes = "";
			String letter = "";
			
			//These are for later on when inputing the letters into the dashes
			int index = 0;
			int index2 = 0;
			int index3 = 0;
			
			//Holds the losses, guesses and dashes left
			int loss = 0;
			int numGuessLeft = 8;
			int numDash = 0;
			
			String replay;
			
			//Strings are used for whole word guesser
			String wordGuessAns;
			String guess;
			
			//Array that holds repeated letters
			String alletter []= new String [1000000];
			int numLetters = 0;
			boolean error=true;
			
			//bob is just a random int that holds a number for the if statements
			int bob=0;
			int bodyPart=0;
			String guessedLetters = "";
			
			//This will print the gallows
			
			//All different difficulties
			String Baby [] = {"banana", "cookie", "eye", "juice", "shoe", "hello", "yes", "no", "car", "ball"};
			String Hard [] = {"arctic", "abjure", "edict", "tirade", "quaint", "zuzim", "buxom", "indict", "liaison", "ecstasy"};
			String Harder [] = {"mendacious", "pugnacious", "aggrandize", "accommodate", "vociferous", "postganglionic", "disciplinarian", "ferromanganese", "ethnohistorians", "organogenetic"};
			String Hardest [] = {"countersurveillances", "antiferromagnetically", "lymphogranulomatoses", "tetrahydrocannabinol", "homotransplantations", "compartmentalization", "establishmentarianism", "disestablishmentarian", "transformationalists", "phosphatidylethanolamines"};
			System.out.println("Next, what difficulty?");
			System.out.println("Baby , Hard  , Harder  ,or Hardest  ");
			String choice;
			
			//Checks your answer for the difficulties
			choice = scan.next();
			while((!choice.equalsIgnoreCase("Baby"))&&(!choice.equalsIgnoreCase("Hard"))&&(!choice.equalsIgnoreCase("Harder"))&&(!choice.equalsIgnoreCase("Hardest"))){
				System.out.println("Invalid answer ");
				System.out.println("Baby , Hard  , Harder  ,or Hardest ");
				choice = scan.next();
			}
			
			//Randomly chooses the category
			if(choice.equalsIgnoreCase("Baby")){
				secretWord = Baby [rand.nextInt(10)];
			}
			else if(choice.equalsIgnoreCase("Hard")){
				secretWord = Hard [rand.nextInt(10)];
			}
			else if(choice.equalsIgnoreCase("Harder")){
				secretWord = Harder [rand.nextInt(10)];
			}
			else if(choice.equalsIgnoreCase("Hardest")){
				secretWord = Hardest [rand.nextInt(10)];
			}
			
			//Gets the word and how long it is for the dashes
			int wordLength = secretWord.length();
			for(int x=0;x<wordLength;x++){
				dashes= dashes+"-";
				
				//numDash holds the number of dashes and that is used for guessing the secret word
				numDash++;
			}
			
			System.out.println("");
			System.out.println("Before you start. Have fun!");
			while(bodyPart != 8 && !dashes.equals(secretWord)){
				error=true;
				
				//These are for guessing the words
				if(numDash<=3 && (choice.equalsIgnoreCase("Hard"))){
					error=true;
					System.out.println("");
					System.out.println("Would you like to guess the word?");
					wordGuessAns = scan.next();
					if(wordGuessAns.equalsIgnoreCase("yes")){
						System.out.println("Ok, guess the word");
						guess = scan.next();
						if(guess.equalsIgnoreCase(secretWord)){
							System.out.println("Wow you have guessed the word");
							dashes = secretWord;
							break;
						}
						else{
							System.out.println("So close, but so far away");
						System.out.println("");
						}
					}
					else{
						System.out.println("Ok");
						System.out.println("");
					}
				}//End of the first word guesser
				else if(numDash<=5 && choice.equalsIgnoreCase("Harder")){
					error=true;
					System.out.println("");
					System.out.println("Would you like to guess the word?");
					wordGuessAns = scan.next();
					if(wordGuessAns.equalsIgnoreCase("yes")){
						System.out.println("Ok, guess the word");
						guess = scan.next();
						if(guess.equalsIgnoreCase(secretWord)){
							System.out.println("Wow you have guessed the word");
							dashes = secretWord;
							break;
						}
						else{
							System.out.println("So close, but so far away");
						System.out.println("");
						}
					}
					else{
						System.out.println("Ok");
						System.out.println("");
					}
				}//End of the second word guesser
				else if(numDash<=10 && choice.equalsIgnoreCase("Hardest")){
					error=true;
					System.out.println("");
					System.out.println("Would you like to guess the word?");
					wordGuessAns = scan.next();
					if(wordGuessAns.equalsIgnoreCase("yes")){
						System.out.println("Ok, guess the word");
						guess = scan.next();
						if(guess.equalsIgnoreCase(secretWord)){
							System.out.println("Wow you have guessed the word");
							dashes = secretWord;
							break;
						}
						else{
							System.out.println("So close, but so far away");
						System.out.println("");
						}
					}
					else{
						System.out.println("Ok");
						System.out.println("");

					}
				}//End of the third word guesser
				while(error==true){
					bob=0;
					
					//These tell you how many wrong guesses and how many you have left
					if(8-numGuessLeft==1){
						System.out.println("You have gotten "+(8-numGuessLeft)+" wrong guess");
					}
					else{
						System.out.println("You have gotten "+(8-numGuessLeft)+" wrong guesses");
					}
					if(numGuessLeft==1){
						System.out.println("You have only "+numGuessLeft+" wrong guess left");
					}
					else{
						System.out.println("You have only "+numGuessLeft+" wrong guesses left");
					}
					
					//Prints the dashes/word and makes you guess the word
					System.out.println(dashes);
					System.out.println("Please enter a lowercase letter");
					letter = scan.next();
					letter.toLowerCase();
					
					//Checks for anything thats not the alphabet
					while(!(letter.equals("a")) &&!(letter.equals("b")) &&!(letter.equals("c")) &&!(letter.equals("d")) &&!(letter.equals("e")) &&!(letter.equals("f")) &&!(letter.equals("g")) &&!(letter.equals("h")) &&!(letter.equals("i")) &&!(letter.equals("j")) &&!(letter.equals("k")) &&!(letter.equals("l")) &&!(letter.equals("m")) &&!(letter.equals("n")) &&!(letter.equals("o")) &&!(letter.equals("p")) &&!(letter.equals("q")) &&!(letter.equals("r")) &&!(letter.equals("s")) &&!(letter.equals("t")) &&!(letter.equals("u")) &&!(letter.equals("v")) &&!(letter.equals("w")) &&!(letter.equals("x")) &&!(letter.equals("y")) &&!(letter.equals("z"))){
						System.out.println("Invalid Answer");
						System.out.println(dashes);
						System.out.println("Please enter a lowercase letter");
						letter = scan.next();
						letter.toLowerCase();
					}
					
					//Checks for repeated letters, but if not it puts it in an array
					index = secretWord.indexOf(letter);
					if(numLetters != 0){
						for(int chk = 0; chk<alletter.length;chk++){
							if(letter.equalsIgnoreCase(alletter[chk])){
								bob = 500;
								chk = 100;
							}
						}
					}
					if(bob==500){
						System.out.println("");
						System.out.println("Error");
						System.out.println("Repeated letter");
						System.out.println("");
						error=true;
					}
					else{
						alletter[numLetters] = letter;
						numLetters++;
						error=false;
					}
					
				}//End of guessing while loop/inner inner while loop
				
				//Puts your correct guessed letter in the dashes/word
				//Can do this up to 3 of the same letter
				if(index != -1){
					dashes = dashes.substring (0, index) + letter + dashes.substring (index + 1);
					guessedLetters = guessedLetters + letter;
					numDash--;
					index2 = secretWord.indexOf(letter, index + 1);
					if(index2 != -1){
						dashes = dashes.substring (0, index2) + letter + dashes.substring (index2 + 1);
						numDash--;
						index3 = secretWord.indexOf(letter, index2 + 1);
						if(index3 != -1){
							dashes = dashes.substring (0, index3) + letter + dashes.substring (index3 + 1);
							numDash--;
						}
					}
					System.out.println("");
					System.out.println("Correct");
					System.out.println(dashes);
					System.out.println("");
				}//End of right if statement
				
				else{
					//Tells you when you get a wrong answer
					guessedLetters = guessedLetters + letter;
					System.out.println("");
					System.out.println("");
					System.out.println("	Wrong answer");
					System.out.println("");
					System.out.println("");
					
					//Builds the boby based out what pen you choose
					bodyPart++;
					numGuessLeft--;
					if(bodyPart==8) {
						loss++;
					}//End of the fifth pen type
					
				}//End of wrong else statement
			}//End of inner while loop
			System.out.println("");
			
			//Tells you when you win
			if(loss==0){
				System.out.println("You have won");
			}
			
			//Tells you when you loss
			else{
				System.out.println("You have loss");
				System.out.println("The word was "+secretWord);
			}
			
			//Checks if you want to play again
			System.out.println("Would you like to play again?");
			System.out.println("Yes or No");
			replay = scan.next();
			if(replay.equalsIgnoreCase("Yes")){
				System.out.println("");
			}
			else if(replay.equalsIgnoreCase("No")){
				playAgain=false;
				System.exit(0);
			}
			else{
				System.out.println("Learn to spell");
				playAgain=false;
				System.exit(0);
			}
		}//End of outer while loop
	}//End of method
}//End of class