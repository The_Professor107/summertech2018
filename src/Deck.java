import java.util.*;
public class Deck {
	private	ArrayList<Card> funCards;
	public Deck(){
		funCards = new ArrayList<Card>();
	}
	public void fillDeck() {
		for(int i=2;i<=10;i++) {
			funCards.add(new Card(i,"Hearts"));
			funCards.add(new Card(i,"Spades"));
			funCards.add(new Card(i,"Clubs"));
			funCards.add(new Card(i,"Diamonds"));
		}
		funCards.add(new Card(11,"Hearts"));
		funCards.add(new Card(11,"Spades"));
		funCards.add(new Card(11,"Clubs"));
		funCards.add(new Card(11,"Diamonds"));
		funCards.add(new Card(12,"Hearts"));
		funCards.add(new Card(12,"Spades"));
		funCards.add(new Card(12,"Clubs"));
		funCards.add(new Card(12,"Diamonds"));
		funCards.add(new Card(13,"Hearts"));
		funCards.add(new Card(13,"Spades"));
		funCards.add(new Card(13,"Clubs"));
		funCards.add(new Card(13,"Diamonds"));
		funCards.add(new Card(14,"Hearts"));
		funCards.add(new Card(14,"Spades"));
		funCards.add(new Card(14,"Clubs"));
		funCards.add(new Card(14,"Diamonds"));
	}
	public ArrayList<Card> getDeck(){
		return this.funCards;
	}
	public Card pickCard() {
		Random rand = new Random();
		return funCards.get(rand.nextInt(funCards.size()));
	}
	public Card getCard(int eye) {
		return funCards.get(eye);
	}
	public int findCard(Card fff) {
		for(int i =0; i<this.funCards.size(); i++) {
			if(fff.equals(funCards.get(i))) {
				return i;
			}
		}
		return -1;
	}
	public int countCards() {
		return funCards.size();
	}
	public void shuffle() {
		ArrayList <Card> temp = new ArrayList<Card>();
		for(int i =0; i<funCards.size();i++) {
			
			temp.add(this.pickCard());
		}
		this.deckClear();
		this.addCards(temp);
	}
	public void addCard(Card addd) {
		funCards.add(addd);
	}
	public void addCards(ArrayList<Card> x) {
		funCards.addAll(x);
	}
	public void deckClear() {
		funCards.clear();
	}
	public void removeCard(Card remove) {
		funCards.remove(this.findCard(remove));
	}
	public void removeIntCard(int removeInt) {
		funCards.remove(removeInt);
	}
	public String toString() {
		String cur = "";
		for(Card card : this.funCards) {
			cur+=card+"\n";
		}
		return cur;
	}
}
