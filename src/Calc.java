import java.util.*;
public class Calc {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		double num1,num2;
		int cho;
		double all=0;
		boolean replay = true;
		String reAnswer;
		while(true) {
			System.out.println("What type of operation?");
			System.out.println("Addition - 1");
			System.out.println("Subtration - 2");
			System.out.println("Multiplaction - 3");
			System.out.println("Divison - 4");
			System.out.println("Remainder - 5");
			System.out.println("Factoral - 6");
			cho = scan.nextInt();
			System.out.print("Enter your first number: ");
			num1 = scan.nextDouble();
			num2 = 0;
			if(cho != 6) {
				System.out.print("Enter your second number: ");
				num2 = scan.nextDouble();
			}
			if(cho ==1){
				all= add(num1,num2);
			}
			else if(cho ==2){
				all=sub(num1,num2);
			}
			else if(cho ==3){
				all=mul(num1,num2);
			}
			else if(cho ==4){
				all=div(num1,num2);
			}
			else if(cho ==5){
				all=mod(num1,num2);
			}
			else if(cho ==6){
				all=fa(num1);
			}
			else {
				System.out.println("Invalid Operation");
			}
			System.out.printf("The answer is %.2f.\n",all);
			System.out.println("Do you want to go again?");
			System.out.println("Yes or no");
			scan.nextLine();
			reAnswer = scan.nextLine();
			if(reAnswer.equalsIgnoreCase("yes")) {
				System.out.println("");
			}
			else{
				System.out.println("Bye");
				replay=false;
			}
		}
	}
	public static double add(double num1,double num2) {
		return num1+num2;
	}
	public static double sub(double num1,double num2) {
		return num1-num2;
	}
	public static double mul(double num1,double num2) {
		return num1*num2;
	}
	public static double div(double num1,double num2) {
		return num1/num2;
	}
	public static double mod(double num1,double num2) {
		return num1%num2;
	}
	public static double fa(double num1) {
		double total = 1;
		for(int i  = 0;i<num1;i++) {
			total *=num1-i;
		}
		return total;
		
	}
}
