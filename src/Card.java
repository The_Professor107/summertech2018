
public class Card {
	private int number;
	private String suit;
	public Card() {
		//Jack = 11
		//Queen = 12
		//King = 13
		//Ace = 14
		this.number = 14;
		this.suit = "Spades";
	}
	public Card (int numbers, String suits) {
		this.number = numbers;
		this.suit = suits;
	}
	public int getNumber(){
		return this.number;
	}
	public String getSuit() {
		return this.suit;
	}
	public void setNumber(int numberee) {
		this.number= numberee;
	}
	public void setSuit(String suitee) {
		this.suit=suitee;
	}
	public boolean equals(Card other) {
		return (this.number == (other.getNumber())) && (this.suit.equals(other.getSuit()));
	}
	public String toString() {
		return this.number+" of "+this.suit;
	}
}
