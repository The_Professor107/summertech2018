
public class Mammal extends Animal{
	private String animalName;
	public Mammal(){
		super(true,false,true,false,true,4,0,2.5,80.0);
		this.animalName = "Paul Blart, Mall Cop 3";
	}
	public Mammal(boolean swimer, boolean tailer, int leger, int armer, double heighter, double weighter,String name) {
		super(swimer,false,tailer,false,true,leger,armer,heighter,weighter);
		this.animalName = name;
	}
	public String getAnName() {
		return this.animalName;
	}
	public void setAnName(String neaeme) {
		this.animalName =neaeme;
	}
	public String toString() {
		return this.animalName+" is a mammal that is "+super.getHeight()+" feet tall and weighs around "+super.getWeight()+" pounds.  It "+(super.getSwim()?"swims":"does not swim")+", it "+(super.getWing()?"can fly":"can not fly")+", it "+(super.getTail()?"has a tail":"does not have a tail")+", it "+(super.getGill()?"has gills":"does not have gills")+", it "+(super.getFur()?"has fur":"does not have fur")+", and finally it has "+super.getArm()+" arms and "+super.getLeg()+" legs.";

	}
}