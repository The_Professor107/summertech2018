import java.util.ArrayList;
public class Wolf extends Mammal {
	private Wolf mate;
	private boolean alive;
	private ArrayList<Wolf> children;
	
	public Wolf() {
		super(true,true,4,0,2.5,97.5,"Bobby Williams the Third King of Earth");
		this.mate = null;
		this.alive = true;
		this.children = new ArrayList<Wolf>();
	}

	public Wolf(boolean swimer, boolean tailer, int leger, int armer, double heighter, double weighter,
			String name, Wolf  mates, boolean alives,ArrayList<Wolf> childrens) {
		super(swimer, tailer, leger, armer, heighter, weighter, name);
		this.mate = mates;
		this.alive = alives;
		this.children = childrens;
	}
	
	public Wolf getMate() {
		return this.mate;
	}
	
	public boolean getAlive() {
		return this.alive;
	}
	
	public ArrayList<Wolf> getChildren() {
		return this.children;
	}
	
	public void setMate(Wolf partner) {
		this.mate = partner;
	}
	
	public void setAlive(boolean living) {
		this.alive = living;
	}
	
	public void addChild(Wolf newKid) {
		this.children.add(newKid);
	}
	
	public void removeChild(Wolf kid) {
		this.children.remove(kid);
	}

	public String toString() {
		return super.getAnName()+" is a WOLF that is "+super.getHeight()+" feet tall and weighs around "+super.getWeight()+" pounds.  It "+(super.getSwim()?"swims":"does not swim")+", it "+(super.getWing()?"can fly":"can not fly")+", it "+(super.getTail()?"has a tail":"does not have a tail")+", it "+(super.getGill()?"has gills":"does not have gills")+", it "+(super.getFur()?"has fur":"does not have fur")+", and finally it has "+super.getLeg()+" legs.";
	}
	
	public void mate(String name, double heighter, double weighter) {
		this.children.add(new Wolf(true,true,4,0,heighter,weighter,name,null,true,new ArrayList<Wolf>()));
	}
}
