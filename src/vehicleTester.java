
public class vehicleTester {

	public static void main(String[] args) {
		Vehicle car = new Vehicle("yellow",4,4,9001);
		System.out.println(car.getColor());
		System.out.println(car.getSeats());
		System.out.println(car.getWheels());
		System.out.println(car.getSpeed());
		car.setColor("Rainbow");
		car.setSeats(1);
		car.setWheels(2);
		car.setSpeed(5);
		System.out.println(car.getColor());
		System.out.println(car.getSeats());
		System.out.println(car.getWheels());
		System.out.println(car.getSpeed());
		car.drive(5.7);
		System.out.println(car.getDrive());
		System.out.println(car);
	}
}
