import java.util.*;
public class VehicleDealership {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Random rand = new Random();
		ArrayList<Vehicle> inStock = new ArrayList<Vehicle>();
		System.out.println("How many cars do you want to see?");
		int numCars = scan.nextInt();
		int numSteels, numSteats;
		String humcolor;
		String listOfColors = "red orange yellow green blue purple rainbow";
		System.out.println("Maxinum number of seats?");
		int maxSeats = scan.nextInt();
		System.out.println("Maxinum number of wheels?");
		int maxWheels = scan.nextInt();
		for(int i =0; i <numCars; i++) {
			int numNumSeats = rand.nextInt(maxSeats)+1;
			int numNumWheels = rand.nextInt(maxWheels)+1;
			int sped = rand.nextInt(1000);
			String[] colors = {"Red","Orange","Yellow","Green","Blue","Purple","Rainbow"};
			inStock.add(new Vehicle(colors[rand.nextInt(colors.length)],numNumSeats,numNumWheels,sped));
		}
		printStock(inStock);
		while(true) {
			System.out.println("Do you have a prefrence? \n Color - 1 \n Number of wheels - 2 \n Number of seats - 3");
			int pref = scan.nextInt();
			if(pref ==1) {
				System.out.println("What type of color?");
				do {
					System.out.println("Red, orange, yellow, green, blue, purple, and rainbow");
					humcolor = scan.next();
				}while(!listOfColors.contains(humcolor.toLowerCase()));
				printStock(searchColor(humcolor,inStock));
				System.out.println("Do you want to filter by number of wheels (1) or number of seats (2)?");
				int choice = scan.nextInt();
				if(choice==1) {
					System.out.println("Enter the number of wheels: ");
					numSteels = scan.nextInt();
					while(numSteels<0 || numSteels>maxWheels+1) {
						System.out.println("Invalid number choose between 1 and "+maxWheels);
						System.out.println("Enter the number of wheels: ");
						numSteels= scan.nextInt();
					}
					printStock(searchWheels(numSteels,searchColor(humcolor,inStock)));
					System.out.println("Do you want to filter the number seats?");
					System.out.println("Yes or No");
					String filter = scan.next();
					if(filter.equalsIgnoreCase("yes")) {
						System.out.println("How many seats?");
						numSteats = scan.nextInt();
						while(numSteats<0 || numSteats>numSteats+1) {
							System.out.println("Invalid number choose between 1 and "+numSteats);
							System.out.println("How many seats");
							numSteats= scan.nextInt();
						}
						printStock(searchSeats(numSteats,searchWheels(numSteels,searchColor(humcolor,inStock))));
					}
					else {
						System.out.println("");
					}
				}
				else if(choice==2) {

					System.out.println("Enter the number of seats: ");
					numSteats= scan.nextInt();
					while(numSteats<0 || numSteats>numSteats+1) {
						System.out.println("Invalid number choose between 1 and "+numSteats);
						System.out.println("How many seats");
						numSteats= scan.nextInt();
					}
					printStock(searchWheels(numSteats,searchColor(humcolor,inStock)));
					System.out.println("Do you want to filter the number wheels?");
					System.out.println("Yes or No");
					String filter = scan.next();
					if(filter.equalsIgnoreCase("yes")) {
						System.out.println("How many wheels?");
						numSteels = scan.nextInt();
						while(numSteels<0 || numSteels>maxWheels+1) {
							System.out.println("Invalid number choose between 1 and "+maxWheels);
							System.out.println("Enter the number of wheels: ");
							numSteels= scan.nextInt();
						}
						printStock(searchSeats(numSteels,searchWheels(numSteats,searchColor(humcolor,inStock))));
					}
					else {
						System.out.println("");
					}
				}
			}

			else if(pref ==2) {
				System.out.println("How many wheels?");
				numSteels = scan.nextInt();
				while(numSteels<0 || numSteels>maxWheels+1) {
					System.out.println("Invalid number choose between 1 and "+maxWheels);
					System.out.println("Enter the number of wheels: ");
					numSteels= scan.nextInt();
				}
				printStock(searchWheels(numSteels,inStock));
				System.out.println("Do you want to filter by colors (1) or number of seats (2)?");
				int choice = scan.nextInt();
				if(choice==1) {
					System.out.println("What color?");
					do {
						System.out.println("Red, orange, yellow, green, blue, purple, and rainbow");
						humcolor = scan.next();
					}while(!listOfColors.contains(humcolor.toLowerCase()));
					printStock(searchColor(humcolor,searchWheels(numSteels,inStock)));
					System.out.println("Do you want to filter the number seats?");
					System.out.println("Yes or No");
					String filter = scan.next();
					if(filter.equalsIgnoreCase("yes")) {
						System.out.println("How many seats?");
						numSteats = scan.nextInt();
						while(numSteats<0 || numSteats>numSteats+1) {
							System.out.println("Invalid number choose between 1 and "+numSteats);
							System.out.println("How many seats");
							numSteats= scan.nextInt();
						}

						printStock(searchSeats(numSteats,searchColor(humcolor,searchWheels(numSteels,inStock))));
					}
					else {
						System.out.println("");
					}
				}
				else if(choice==2) {

					System.out.println("Enter the number of seats: ");
					numSteats= scan.nextInt();
					while(numSteats<0 || numSteats>numSteats+1) {
						System.out.println("Invalid number choose between 1 and "+numSteats);
						System.out.println("How many seats");
						numSteats= scan.nextInt();
					}
					printStock(searchSeats(numSteats,searchWheels(numSteels,inStock)));
					System.out.println("Do you want to filter by color?");
					System.out.println("Yes or No");
					String filter = scan.next();
					if(filter.equalsIgnoreCase("yes")) {
						System.out.println("What color?");
						do {
							System.out.println("Red, orange, yellow, green, blue, purple, and rainbow");
							humcolor = scan.next();
						}while(!listOfColors.contains(humcolor.toLowerCase()));
						printStock(searchColor(humcolor,searchSeats(numSteats,searchWheels(numSteels,inStock))));
					}
					else {
						System.out.println("");
					}
				}
			}

			else if(pref ==3) {
				System.out.println("How many seats");
				numSteats = scan.nextInt();
				while(numSteats<0 || numSteats>numSteats+1) {
					System.out.println("Invalid number choose between 1 and "+numSteats);
					System.out.println("How many seats");
					numSteats= scan.nextInt();
				}
				printStock(searchSeats(numSteats,inStock));
				System.out.println("Do you want to filter by number of colors (1) or number of wheels (2)?");
				int choice = scan.nextInt();
				if(choice==1) {
					System.out.println("What color?");
					do {
						System.out.println("Red, orange, yellow, green, blue, purple, and rainbow");
						humcolor = scan.next();
					}while(!listOfColors.contains(humcolor.toLowerCase()));
					printStock(searchColor(humcolor,searchSeats(numSteats,inStock)));
					System.out.println("Do you want to filter the number seats?");
					System.out.println("Yes or No");
					String filter = scan.next();
					if(filter.equalsIgnoreCase("yes")) {
						System.out.println("How many wheels?");
						numSteels = scan.nextInt();
						while(numSteels<0 || numSteels>maxWheels+1) {
							System.out.println("Invalid number choose between 1 and "+maxWheels);
							System.out.println("Enter the number of wheels: ");
							numSteels= scan.nextInt();
						}
						printStock(searchWheels(numSteels,searchColor(humcolor,searchSeats(numSteats,inStock))));
					}
					else {
						System.out.println("");
					}
				}
				else if(choice==2) {
					System.out.println("Enter the number of wheels: ");
					numSteels= scan.nextInt();
					while(numSteels<0 || numSteels>maxWheels+1) {
						System.out.println("Invalid number choose between 1 and "+maxWheels);
						System.out.println("Enter the number of wheels: ");
						numSteels= scan.nextInt();
					}
					printStock(searchWheels(numSteels,searchSeats(numSteats,inStock)));
					System.out.println("Do you want to filter by color?");
					System.out.println("Yes or No");
					String filter = scan.next();
					if(filter.equalsIgnoreCase("yes")) {
						System.out.println("What color?");
						do {
							System.out.println("Red, orange, yellow, green, blue, purple, and rainbow");
							humcolor = scan.next();
						}while(!listOfColors.contains(humcolor.toLowerCase()));
						printStock(searchColor(humcolor,searchWheels(numSteels,searchSeats(numSteats,inStock))));
					}
					else {
						System.out.println("");
					}
				}
			}
			System.out.println("Do you want to go through your cars again?");
			System.out.println("Yes or No");
			String answer=scan.next();
			if(answer.equalsIgnoreCase("no")) {
				break;
			}
		}
	}

	public static void printStock(ArrayList<Vehicle> stock) {
		for(int i=0; i<stock.size(); i++) {
			System.out.println("Car number "+(i+1)+stock.get(i));
		}
	}
	public static ArrayList<Vehicle> searchColor(String humcolor, ArrayList<Vehicle> stock) {
		ArrayList<Vehicle> matches = new ArrayList<Vehicle>();
		for(int i=0; i<stock.size(); i++) {
			if(stock.get(i).getColor().equalsIgnoreCase(humcolor)) {
				matches.add(stock.get(i));
			}
		}

		return matches;
	}
	public static ArrayList<Vehicle> searchWheels(int numStuff, ArrayList<Vehicle> stock) {
		ArrayList<Vehicle> matches = new ArrayList<Vehicle>();
		for(int i=0; i<stock.size(); i++) {
			if(stock.get(i).getWheels()==numStuff) {
				matches.add(stock.get(i));
			}
		}
		return matches;
	}
	public static ArrayList<Vehicle> searchSeats(int numStuff, ArrayList<Vehicle> stock) {
		ArrayList<Vehicle> matches = new ArrayList<Vehicle>();
		for(int i=0; i<stock.size(); i++) {
			if(stock.get(i).getSeats()==numStuff) {
				matches.add(stock.get(i));
			}
		}
		return matches;
	}
}
